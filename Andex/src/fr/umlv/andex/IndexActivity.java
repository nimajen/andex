package fr.umlv.andex;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import fr.umlv.andex.communication.AndexServerCommunicator;
import fr.umlv.andex.elements.AnswerExam;
import fr.umlv.andex.elements.Exam;
import fr.umlv.andex.parser.Parser;
import fr.umlv.andex.user.AndexUser;

public class IndexActivity extends Activity {

	private AndexUser user;
	private AndexServerCommunicator communicator;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_index);

		user = (AndexUser) getIntent().getSerializableExtra("user");
		communicator = (AndexServerCommunicator) getIntent()
				.getSerializableExtra("communicator");
		final ListView listView = (ListView) findViewById(R.id.listView1);
		String[] values = user.getAvailableExams().toArray(new String[0]);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, values);

		// Assign adapter to ListView
		listView.setAdapter(adapter);

		// call exam activity with the specific exam for click on list element
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					final int pos, long arg3) {
				Thread thread = new Thread(new Runnable() {

					@Override
					public void run() {
						Intent intent = new Intent(IndexActivity.this,
								ExamActivity.class);
						intent.putExtra("communicator", communicator);
						intent.putExtra("user", user);
						Exam exam = getExam((String) listView
								.getItemAtPosition(pos));
						intent.putExtra("exam", exam);
						intent.putExtra(
								"answer",
								getAnswer(exam, (String) listView
										.getItemAtPosition(pos)));
						startActivity(intent);
					}
				});
				thread.start();
			}
		});

	}

	private Exam getExam(String name) {

		String directory = new File(getApplicationContext().getFilesDir()
				.getPath(), name).getAbsolutePath();

		// Unzip the exam
		communicator.unzip(communicator.getExam(user, name), directory);

		FileInputStream streamExam = communicator.getExamFile(directory);

		Exam exam = null;
		try {
			exam = Parser.parseExam(streamExam, name);
		} catch (IOException e) {
			try {
				streamExam.close();
			} catch (IOException e1) {
			}
		} catch (XmlPullParserException e) {
			try {
				streamExam.close();
			} catch (IOException e1) {
			}
		}
		return exam;
	}

	private AnswerExam getAnswer(Exam exam, String name) {

		// The following directory mist exists, i.e. the zip has to be unzipped.
		// The method getExam must be call first
		String directory = new File(getApplicationContext().getFilesDir()
				.getPath(), name).getAbsolutePath();

		// If streamAnswer is null, there is no correction for the exam
		FileInputStream streamAnswer = communicator.getAnswerFile(directory);

		if (streamAnswer == null) {
			return null;
		}

		AnswerExam answer = null;
		try {
			answer = Parser.parseAnswerExam(streamAnswer, exam, name);
		} finally {
			try {
				streamAnswer.close();
			} catch (IOException e) {
				// nothing to do
			}
		}
		return answer;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.activity_index, menu);
		return true;
	}

}
