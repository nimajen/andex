package fr.umlv.andex.user;

import java.io.Serializable;
import java.util.List;

public class AndexUser implements Serializable{

	private static final long serialVersionUID = -7533372335652649043L;
	private final String auth;
	private List<String> availableExams;

	public AndexUser(String auth, List<String> availableExams) {
		this.auth = auth;
		this.availableExams = availableExams;
	}

	public List<String> getAvailableExams() {
		return availableExams;
	}

	public void setAvailableExams(List<String> availableExams) {
		this.availableExams = availableExams;
	}

	public String getAuth() {
		return auth;
	}

	@Override
	public String toString() {
		return "AndexUser [auth=" + auth + ", availableExams=" + availableExams
				+ "]";
	}

}
