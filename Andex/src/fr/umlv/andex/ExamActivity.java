package fr.umlv.andex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import fr.umlv.andex.communication.AndexServerCommunicator;
import fr.umlv.andex.elements.AnswerExam;
import fr.umlv.andex.elements.Exam;
import fr.umlv.andex.elements.MCQ;
import fr.umlv.andex.elements.MCQAnswer;
import fr.umlv.andex.elements.Part;
import fr.umlv.andex.elements.Question;
import fr.umlv.andex.elements.QuestionList;
import fr.umlv.andex.user.AndexUser;

public class ExamActivity extends Activity {

	private AndexUser user;
	private AnswerExam answerExam;
	private AndexServerCommunicator communicator;
	private Handler handler = new Handler();

	@Override
	public void onBackPressed() {
		// do nothing to avoid an accidental return
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_exam);
		user = (AndexUser) getIntent().getSerializableExtra("user");
		final Exam exam = (Exam) getIntent().getSerializableExtra("exam");

		communicator = (AndexServerCommunicator) getIntent()
				.getSerializableExtra("communicator");
		answerExam = new AnswerExam(exam.getName());

		final AnswerExam correctedExam = (AnswerExam) getIntent()
				.getSerializableExtra("answer");

		final HashMap<String, EditText> simpleAnswers = new HashMap<String, EditText>();

		final TableLayout tl = new TableLayout(this);
		TableRow tr = new TableRow(this);
		TableRow tr2 = new TableRow(this);
		TableRow tr3 = new TableRow(this);
		TableRow trQList = new TableRow(this);
		TableRow trPart = new TableRow(this);
		TextView partTitle = new TextView(this);
		TextView questionListTitle = new TextView(this);
		TextView question = new TextView(this);
		EditText answer = new EditText(this);
		TextView empty = new TextView(this);
		CheckBox qcmBaseGroup = new CheckBox(this);
		Button qcmBaseButton = new Button(this);
		tl.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT));
		tl.setStretchAllColumns(true);

		final List<String> listId = new ArrayList<String>();
		for (Part part : exam.getList()) {
			trPart = new TableRow(this);
			partTitle = new TextView(this);
			partTitle
					.setText(Html.fromHtml("<h2>" + part.getTitle() + "</h2>"));
			trPart.addView(partTitle);
			tl.addView(trPart);

			for (QuestionList questionList : part.getList()) {

				trQList = new TableRow(this);
				questionListTitle = new TextView(this);
				// questionListTitle.setText(questionList.getTitle());
				questionListTitle.setText(Html.fromHtml("<b><i>"
						+ questionList.getTitle() + "</i></b><br />"));
				trQList.addView(questionListTitle);
				tl.addView(trQList);

				for (Question questionL : questionList.getList()) {

					// MCQ question
					if (questionL instanceof MCQ) {

						final MCQ qcm = (MCQ) questionL;

						final String qcmId = qcm.getId();
						listId.add(qcmId);
						tr = new TableRow(this);
						tr2 = new TableRow(this);
						tr3 = new TableRow(this);

						question = new TextView(this);
						List<MCQAnswer> answers = qcm.getChoices();

						empty = new TextView(this);

						/* Create a new row to be added. */
						question.setText(Html.fromHtml("<b>"
								+ questionL.getStatement() + "</b>"));
						tr.addView(question);

						answerExam.addMCQ(qcmId);
						for (final MCQAnswer mcqAnswer : answers) {
							final String idMCQAnswer = mcqAnswer.getId();
							qcmBaseGroup = new CheckBox(this);
							qcmBaseGroup
									.setOnCheckedChangeListener(new OnCheckedChangeListener() {

										@Override
										public void onCheckedChanged(
												CompoundButton buttonView,
												boolean isChecked) {
											if (isChecked) {
												answerExam.answerMCQ(qcmId,
														idMCQAnswer);
											} else {
												answerExam.unanswerMCQ(qcmId,
														idMCQAnswer);
											}

										}
									});

							qcmBaseGroup.append(mcqAnswer.getValue());
							tr2.addView(qcmBaseGroup);
						}

						tr3.addView(empty);

						/* Add row to TableLayout. */
						tl.addView(tr);
						tl.addView(tr2);
						tl.addView(tr3);

					}
					// simple text question
					else {

						final String idQuestion = questionL.getId();
						answerExam.addSimpleQuestion(idQuestion);
						listId.add(idQuestion);
						tr = new TableRow(this);
						tr2 = new TableRow(this);
						tr3 = new TableRow(this);

						question = new TextView(this);
						answer = new EditText(this);
						simpleAnswers.put(idQuestion, answer);
						empty = new TextView(this);

						/* Create a new row to be added. */
						question.setText(Html.fromHtml("<b>"
								+ questionL.getStatement() + "</b>"));
						tr.addView(question);

						tr2.addView(answer);

						tr3.addView(empty);

						/* Add row to TableLayout. */
						tl.addView(tr);
						tl.addView(tr2);
						tl.addView(tr3);
					}
				}
			}
		}

		Button summit = (Button) findViewById(R.id.ExamActivity_buttonSubmit);
		if (correctedExam != null) {
			System.out.println("CORRECTION !");
			summit.setText(R.string.correctExam);
		}
		summit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				for (Entry<String, EditText> entry : simpleAnswers.entrySet()) {
					String value = entry.getValue().getText().toString();
					answerExam.answerSimpleQuestion(entry.getKey(), value);
				}
				Thread sendThread = new Thread(new Runnable() {

					@Override
					public void run() {
						// no correction : send the exam
						if (correctedExam == null) {

							if (!communicator.sendExam(user, answerExam)) {
								final Context context = getApplicationContext();

								final Runnable messageSendFailed = new Runnable() {
									public void run() {
										Toast.makeText(context,
												R.string.failedSend,
												Toast.LENGTH_LONG).show();
									}
								};
								handler.post(messageSendFailed);
							} else {
								finish();
							}
						}
						// There is a correction
						else {
							Intent intent = new Intent(ExamActivity.this,
									AnswerActivity.class);
							intent.putExtra("user", user);
							intent.putExtra("exam", exam);
							intent.putExtra("answer", correctedExam);
							startActivity(intent);
						}

					}
				});
				sendThread.start();
			}
		});

		/**
		 * Ajout dans le layout de notre tableau
		 */
		LinearLayout ln = (LinearLayout) findViewById(R.id.lineForTab);
		ln.addView(tl);

		Button cancel = (Button) findViewById(R.id.ExamActivity_buttonReturn);
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						ExamActivity.this);
				builder.setPositiveButton(R.string.cancelExam,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								finish();
							}
						});
				builder.setNegativeButton(R.string.continueExam,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
				builder.setMessage(R.string.confirmCancelExam);
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.activity_exam, menu);
		return true;
	}

}
