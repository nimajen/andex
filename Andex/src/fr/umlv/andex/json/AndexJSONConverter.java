package fr.umlv.andex.json;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import fr.umlv.andex.user.AndexUser;

public class AndexJSONConverter {

	public static AndexUser createAndexUserFromJSonData(String jsonData) {
		JSONTokener tokener = new JSONTokener(jsonData);
		JSONObject result = null;
		try {
			result = new JSONObject(tokener);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String auth;
		try {
			auth = (String) result.get("token");
		} catch (JSONException e) {
			return null;
		}
		JSONArray arrayExam;
		try {
			arrayExam = (JSONArray) result.get("exams");
		} catch (JSONException e) {
			return null;
		}
		List<String> exams = new ArrayList<String>();
		int len = arrayExam.length();
		for (int i = 0; i < len; i++) {
			try {
				exams.add(arrayExam.getString(i));
			} catch (JSONException e) {
				return null;
			}
		}
		AndexUser user = new AndexUser(auth, exams);
		return user;
	}
}
