package fr.umlv.andex;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import fr.umlv.andex.communication.AndexServerCommunicator;
import fr.umlv.andex.user.AndexUser;

public class LoginActivity extends Activity {

	private AndexServerCommunicator communicator;
	// = new AndexServerCommunicator(
	// "http://10.0.2.2:6112");
	// "http://10.2.26.14:6112");

	private EditText loginField;
	private EditText passwordField;
	private AndexUser user;
	private EditText url;
	final Handler handler = new Handler();

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		loginField = (EditText) findViewById(R.id.Login_email);
		passwordField = (EditText) findViewById(R.id.Login_password);
		url = (EditText) findViewById(R.id.Login_url);

		findViewById(R.id.Login_sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						Thread t = new Thread(new Runnable() {

							@Override
							public void run() {
								System.out.println(url.getText().toString());
								communicator = new AndexServerCommunicator(url
										.getText().toString());
								user = communicator.connect(loginField
										.getText().toString(), passwordField
										.getText().toString());
								if (user != null) {
									Intent intent = new Intent(
											LoginActivity.this,
											IndexActivity.class);
									intent.putExtra("user", user);
									intent.putExtra("communicator",
											communicator);
									startActivity(intent);
								} else {
									final Context context = getApplicationContext();

									final Runnable messageServerNotRunning = new Runnable() {
										public void run() {
											Toast.makeText(context,
													R.string.notRunning,
													Toast.LENGTH_LONG).show();
										}
									};
									handler.post(messageServerNotRunning);
								}
							}
						});
						t.start();
					}
				});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.activity_login, menu);
		return true;
	}
}
