package fr.umlv.andex.parser;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Scanner;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import fr.umlv.andex.elements.AnswerExam;
import fr.umlv.andex.elements.Exam;
import fr.umlv.andex.elements.MCQ;
import fr.umlv.andex.elements.MCQAnswer;
import fr.umlv.andex.elements.Part;
import fr.umlv.andex.elements.Question;
import fr.umlv.andex.elements.QuestionList;
import fr.umlv.andex.elements.SimpleQuestion;

public class Parser {
	// public static Exam parse(Context context, String name) throws
	// XmlPullParserException, IOException {

	// System.out.println("PARSE: " + name);

	// InputStream inputStream = context.getAssets().open(name);

	public static Exam parseExam(InputStream inputStream, String name)
			throws XmlPullParserException, IOException {
		Exam exam = new Exam(name);
		XmlPullParserFactory xppf = XmlPullParserFactory.newInstance();
		XmlPullParser xpp = xppf.newPullParser();
		xpp.setInput(inputStream, "UTF-8");
		int eventType = xpp.getEventType();

		Part lastPart = null;
		QuestionList lastQL = null;
		MCQ lastMCQ = null;

		while (eventType != XmlPullParser.END_DOCUMENT) {
			switch (eventType) {
			case XmlPullParser.START_TAG: {
				String xppName = xpp.getName();

				if (xppName.equals("exam")) {
					exam.setId(getId(xpp));
					exam.setTitle(getValue(xpp));
				} else if (xppName.equals("part")) {
					Part part = new Part();
					part.setId(getId(xpp));
					part.setTitle(getValue(xpp));
					exam.add(part);
					lastPart = part;
				} else if (xppName.equals("question-list")) {
					QuestionList ql = new QuestionList();
					ql.setId(getId(xpp));
					ql.setTitle(getValue(xpp));
					lastPart.add(ql);
					lastQL = ql;
				} else if (xppName.equals("mcq-question")) {
					MCQ mcq = new MCQ();
					mcq.setId(getId(xpp));
					mcq.setStatement(getValue(xpp));
					lastQL.add(mcq);
					lastMCQ = mcq;
				} else if (xppName.equals("mcq-answer")) {
					MCQAnswer answer = new MCQAnswer();
					answer.setId(getId(xpp));
					answer.setValue(getValue(xpp));
					lastMCQ.addChoice(answer);
				} else if (xppName.equals("question-text")) {
					SimpleQuestion question = new SimpleQuestion();
					question.setId(getId(xpp));
					question.setStatement(getValue(xpp));
					lastQL.add(question);
				}

				break;
			}
			case XmlPullParser.END_TAG: {
				String xppName = xpp.getName();

				if (xppName.equals("part")) {
					lastPart = null;
				} else if (xppName.equals("question-list")) {
					lastQL = null;
				} else if (xppName.equals("mcq-question")) {
					lastMCQ = null;
				}

				break;
			}
			default:
				break;
			}
			eventType = xpp.next();
		}
		return exam;
	}

	private static String getValue(XmlPullParser xpp) {
		return xpp.getAttributeValue(null, "value");
	}

	private static String getId(XmlPullParser xpp) {
		return xpp.getAttributeValue(null, "id");
	}

	public static AnswerExam parseAnswerExam(InputStream inputStream,
			Exam exam, String name) {

		AnswerExam answer = new AnswerExam(name);
		Scanner sc = new Scanner(inputStream);
		List<Part> parts = exam.getList();
		for (Part part : parts) {
			for (QuestionList questionsList : part.getList()) {
				for (Question question : questionsList.getList()) {
					// malformed correct file
					if (!sc.hasNextLine()) {
						// throw something ?
						return null;
					}
					String currentLine = sc.nextLine();
					int index = currentLine.indexOf(":");
					// malformed question
					if (index == -1) {
						return null;
					}
					String id = currentLine.substring(0, index);
					String answerText;
					if (currentLine.length() == index + 1) {
						answerText = "";
					} else {
						answerText = currentLine.substring(index + 1);
					}
					// mcq question
					if (question instanceof MCQ) {
						answer.addMCQ(id);
						// one answer
						if (!answerText.contains(",")) {
							answer.answerMCQ(id, answerText);
						} else {
							// many answer
							for (String str : answerText.split(",")) {
								answer.answerMCQ(id, str);
							}
						}
					}
					// simple question
					else {
						answer.addSimpleQuestion(id);
						answer.answerSimpleQuestion(id, answerText);
					}
				}
			}
		}
		return answer;

	}
}