package fr.umlv.andex;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import fr.umlv.andex.elements.AnswerExam;
import fr.umlv.andex.elements.Exam;
import fr.umlv.andex.elements.MCQ;
import fr.umlv.andex.elements.MCQAnswer;
import fr.umlv.andex.elements.Part;
import fr.umlv.andex.elements.Question;
import fr.umlv.andex.elements.QuestionList;

public class AnswerActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_answer);

		// user = (AndexUser) getIntent().getSerializableExtra("user");
		Exam exam = (Exam) getIntent().getSerializableExtra("exam");
		AnswerExam answerExam = (AnswerExam) getIntent().getSerializableExtra(
				"answer");

		final TableLayout tl = new TableLayout(this);
		TableRow tr = new TableRow(this);
		TableRow tr2 = new TableRow(this);
		TableRow tr3 = new TableRow(this);
		TableRow trQList = new TableRow(this);
		TableRow trPart = new TableRow(this);
		TextView partTitle = new TextView(this);
		TextView questionListTitle = new TextView(this);
		TextView question = new TextView(this);
		TextView answer = new TextView(this);
		TextView empty = new TextView(this);
		CheckBox qcmBaseGroup = new CheckBox(this);
		Button qcmBaseButton = new Button(this);
		tl.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT));
		tl.setStretchAllColumns(true);

		final List<String> listId = new ArrayList<String>();
		for (Part part : exam.getList()) {
			trPart = new TableRow(this);
			partTitle = new TextView(this);
			partTitle
					.setText(Html.fromHtml("<h2>" + part.getTitle() + "</h2>"));
			trPart.addView(partTitle);
			tl.addView(trPart);

			for (QuestionList questionList : part.getList()) {

				trQList = new TableRow(this);
				questionListTitle = new TextView(this);
				questionListTitle.setText(Html.fromHtml("<b><i>"
						+ questionList.getTitle() + "</i></b><br />"));
				trQList.addView(questionListTitle);
				tl.addView(trQList);

				for (Question questionL : questionList.getList()) {

					// MCQ question
					if (questionL instanceof MCQ) {

						final MCQ qcm = (MCQ) questionL;

						final String qcmId = qcm.getId();
						listId.add(qcmId);
						tr = new TableRow(this);
						tr2 = new TableRow(this);
						tr3 = new TableRow(this);

						question = new TextView(this);
						List<MCQAnswer> answers = qcm.getChoices();
						empty = new TextView(this);

						/* Create a new row to be added. */
						question.setText(Html.fromHtml("<b>"
								+ questionL.getStatement() + "</b>"));
						tr.addView(question);
						answer = new TextView(this);
						String answerId = answerExam.getElementById(qcmId);

						// simple answer for the qcm
						if (!answerId.contains(",")) {
							String questionText = getMCQAnswerTextFromMCQAnswerList(
									answers, answerId);
							answer.setText(questionText);
						} else {
							String[] ids = answerId.split(",");
							StringBuilder result = new StringBuilder();
							int size = ids.length;

							for (int i = 0; i < size; i++) {
								String questionText = getMCQAnswerTextFromMCQAnswerList(
										answers, ids[i]);
								result.append(questionText);
								if (i < size - 1) {
									result.append(" | ");
								}
							}
							answer.setText(result);
						}
						tr2.addView(answer);

						tr3.addView(empty);

						/* Add row to TableLayout. */
						tl.addView(tr);
						tl.addView(tr2);
						tl.addView(tr3);

					}
					// simple text question
					else {

						final String idQuestion = questionL.getId();
						listId.add(idQuestion);
						tr = new TableRow(this);
						tr2 = new TableRow(this);
						tr3 = new TableRow(this);

						question = new TextView(this);
						empty = new TextView(this);
						answer = new TextView(this);

						answer.setText(answerExam.getElementById(idQuestion));

						tr2.addView(answer);

						/* Create a new row to be added. */
						question.setText(Html.fromHtml("<b>"
								+ questionL.getStatement() + "</b>"));
						tr.addView(question);

						tr3.addView(empty);

						/* Add row to TableLayout. */
						tl.addView(tr);
						tl.addView(tr2);
						tl.addView(tr3);
					}
				}
			}
		}
		LinearLayout ln = (LinearLayout) findViewById(R.id.lineForTabEAA);
		ln.addView(tl);
	}

	private String getMCQAnswerTextFromMCQAnswerList(List<MCQAnswer> list,
			String answerId) {
		String result = null;
		for (MCQAnswer answer : list) {
			if (answer.getId().equals(answerId)) {
				result = answer.getValue();
				break;
			}
		}
		return result;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.activity_answer, menu);
		return true;
	}

}
