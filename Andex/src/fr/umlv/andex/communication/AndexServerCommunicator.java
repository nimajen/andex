package fr.umlv.andex.communication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;

import fr.umlv.andex.elements.AnswerExam;
import fr.umlv.andex.json.AndexJSONConverter;
import fr.umlv.andex.user.AndexUser;

/**
 * Performs communication between the client and the andex server
 * 
 * @author Benjamin Roy
 * 
 */
public class AndexServerCommunicator implements Serializable {

	private static final long serialVersionUID = -6684115259477388201L;
	private final String url;

	public AndexServerCommunicator(String url) {
		this.url = url;
	}

	private static HttpResponse performGetRequest(String requestURL)
			throws IOException {

		HttpGet httpGet = new HttpGet(requestURL);
		HttpClient client = new DefaultHttpClient();
		HttpResponse response = client.execute(httpGet);
		return response;
	}

	private static HttpResponse performPostRequest(String url, String param)
			throws ClientProtocolException, IOException {

		HttpPost httpost = new HttpPost(url);
		StringEntity se = new StringEntity(param, HTTP.UTF_8);
		httpost.setEntity(se);
		httpost.setHeader("Content-type", "application/x-www-form-urlencoded");

		HttpClient client = new DefaultHttpClient();
		HttpResponse response = client.execute(httpost);

		return response;
	}

	/**
	 * Return the json string containing the auth key and all the available exam
	 * for the user
	 * 
	 * @param url
	 * @param username
	 * @param password
	 * @return
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	private String sendConnect(String username, String password)
			throws ClientProtocolException, IOException {
		String param = String.format("username=%s&password=%s", username,
				password);
		HttpResponse response;
		response = performPostRequest(url + "/connect", param);
		if (response.getStatusLine().getStatusCode() != 200) {
			return null;
		}

		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(response
					.getEntity().getContent(), "UTF-8"));
			String line = reader.readLine();
			return line;
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {

			}
		}
	}

	public AndexUser connect(String user, String password) {
		String jsonData;
		try {
			jsonData = sendConnect(user, password);
		} catch (ClientProtocolException e) {
			return null;
		} catch (IOException e) {
			return null;
		} catch (IllegalArgumentException e) {
			return null;
		}
		if (jsonData == null) {
			return null;
		}
		return AndexJSONConverter.createAndexUserFromJSonData(jsonData);
	}

	public boolean sendExam(AndexUser user, AnswerExam exam) {
		String param = String.format("token=%s&exam=%s&data=%s",
				user.getAuth(), exam.getExamName(), exam.getContent());
		HttpResponse response;
		try {
			response = performPostRequest(url + "/submit", param);
		} catch (ClientProtocolException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
		if (response.getStatusLine().getStatusCode() != 200) {
			return false;
		}
		return true;
	}

	public ZipInputStream getExam(AndexUser user, String examName) {
		StringBuilder request = new StringBuilder();
		request.append(url);
		request.append("/exam?token=");
		request.append(user.getAuth());
		request.append("&exam=");
		request.append(examName);
		HttpResponse response = null;
		try {
			response = performGetRequest(request.toString());
		} catch (IOException e) {
			return null;
		}
		if (response.getStatusLine().getStatusCode() != 200) {
			return null;
		}
		ZipInputStream zis = null;
		try {
			zis = new ZipInputStream(response.getEntity().getContent());
		} catch (IllegalStateException e) {
			return null;
		} catch (IOException e) {
			return null;
		}

		return zis;
	}

	public void unzip(ZipInputStream zis, String directory) {
		ZipEntry ze = null;
		try {
			try {
				while ((ze = zis.getNextEntry()) != null) {
					File f = new File(directory, ze.getName());
					// ignore .DS_STORE and other useless file
					if (f.getName().startsWith(".")) {
						continue;
					}
					f.getParentFile().mkdirs();
					FileOutputStream fos = new FileOutputStream(f);
					try {
						try {
							final byte[] buffer = new byte[8192];
							int bytesRead;
							while (-1 != (bytesRead = zis.read(buffer)))
								fos.write(buffer, 0, bytesRead);
						} finally {
							fos.close();
						}
					} catch (IOException ioe) {
						f.delete();
					}
				}
			} catch (FileNotFoundException e) {
			} catch (IOException e) {
			}
		} finally {
			try {
				zis.close();
			} catch (IOException e) {
			}
		}
	}

	public FileInputStream getExamFile(String directory) {
		FileInputStream exam = null;
		try {
			exam = new FileInputStream(directory + "/" + "exam.xml");
		} catch (FileNotFoundException e) {
		}
		return exam;
	}

	public FileInputStream getAnswerFile(String directory) {
		FileInputStream exam = null;
		try {
			exam = new FileInputStream(directory + "/" + "answer.andex");
		} catch (FileNotFoundException e) {
			return null;
		}
		return exam;
	}

}
