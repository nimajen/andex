package fr.umlv.andex.elements;

/**
 * Represents a simple question with a simple field to answer it.
 *
 */
public class SimpleQuestion extends AbstractQuestion{
	
	private static final long serialVersionUID = -740438881632453710L;

	public SimpleQuestion() {
		super();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[Question = ").append(getId());
		sb.append(", ").append(getStatement()).append("]");

		return sb.toString();
	}
	
}
