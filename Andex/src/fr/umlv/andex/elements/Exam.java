package fr.umlv.andex.elements;

public class Exam extends ExamElement<Part> {

	private static final long serialVersionUID = -7625449279784803200L;
	private final String name;

	public Exam(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}