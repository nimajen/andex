package fr.umlv.andex.elements;

import java.io.Serializable;

/**
 * Interface for all questions.
 * 
 */
public interface Question extends Serializable {
	/**
	 * Get the statement of the question.
	 * @return The statement of the question
	 */
	public String getStatement();
	
	/**
	 * Get the id of the question.
	 * @return The id of the question
	 */
	public String getId();

	/**
	 * TODO
	 * @param statement
	 */
	void setStatement(String statement);

	/**
	 * TODO
	 * @param id
	 */
	void setId(String id);
}
