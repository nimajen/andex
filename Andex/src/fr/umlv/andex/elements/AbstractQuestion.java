package fr.umlv.andex.elements;

public class AbstractQuestion implements Question {
	
	private static final long serialVersionUID = 6862451838494357793L;
	private String statement;
	private String id;

	protected AbstractQuestion(String id, String statement) {
		this.id = id;
		this.statement = statement;
	}
	
	protected AbstractQuestion() {
		statement = null;
		id = null;
	}
	
	@Override
	public String getStatement() {
		return statement;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setStatement(String statement) {
		this.statement = statement;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}
}
