package fr.umlv.andex.elements;

import java.util.ArrayList;
import java.util.List;

public class MCQ extends AbstractQuestion {
	
	private static final long serialVersionUID = -3471631925469131936L;
	private final List<MCQAnswer> choices;

	public MCQ() {
		super();
		choices = new ArrayList<MCQAnswer>();
	}

	public List<MCQAnswer> getChoices() {
		return choices;
	}

	public void addChoice(MCQAnswer choice) {
		choices.add(choice);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[MCQ = ").append(getId());
		sb.append(", ").append(getStatement()).append(", ").append(choices).append("]");
		return sb.toString();
	}
}
