package fr.umlv.andex.elements;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ExamElement<T> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5750527690274091861L;
	private List<T> list;
	private String id;
	private String title;
	
	public ExamElement() {
		list = new ArrayList<T>();
	}

	public List<T> getList() {
		return list;
	}

	public void add(T element) {
		list.add(element);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@Override
	public String toString() {
		String className = this.getClass().getSimpleName();
		StringBuilder sb = new StringBuilder();
		sb.append("[").append(className).append(" = ").append(id);
		sb.append(", ").append(title).append(", ").append(list).append("]");
		
		return sb.toString();
	}
}
