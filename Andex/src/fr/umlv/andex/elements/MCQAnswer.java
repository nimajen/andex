package fr.umlv.andex.elements;

import java.io.Serializable;

public class MCQAnswer implements Serializable {
	
	private static final long serialVersionUID = -450478949094033787L;
	private String id = null;
	private String value = null;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[MCQAnswer = ").append(id);
		sb.append(", ").append(value).append("]");
		return sb.toString();
	}
}
