package fr.umlv.andex.elements;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;

public class AnswerExam implements Serializable {

	private static final long serialVersionUID = 5217400788074607771L;
	private final LinkedHashMap<String, String> answers = new LinkedHashMap<String, String>();
	private final String examName;

	public AnswerExam(String examName) {
		this.examName = examName;
	}

	public void addSimpleQuestion(String idQuestion) {
		answers.put(idQuestion, "");
	}

	public void answerSimpleQuestion(String idQuestion, String answer) {
		answers.put(idQuestion, answer);
	}

	public void addMCQ(String idMCQ) {
		answers.put(idMCQ, "");
	}

	public void answerMCQ(String idMCQ, String idAnswer) {
		String old = answers.get(idMCQ);
		if (old.isEmpty()) {
			answers.put(idMCQ, idAnswer);
		} else {
			answers.put(idMCQ, old + "," + idAnswer);
		}
	}

	public void unanswerMCQ(String idMCQ, String idAnswer) {
		String answer = answers.get(idMCQ);
		if (answer.equals(idAnswer)) {
			answers.put(idMCQ, "");
			return;
		}
		if (answer.endsWith(idAnswer)) {
			answer.replace(idAnswer, "");
		} else {
			answer.replace(idAnswer + ",", "");
		}
		answers.put(idMCQ, answer);
	}

	public String getElementById(String key) {
		return answers.get(key);
	}

	public String getContent() {
		StringBuilder answer = new StringBuilder();
		Set<Entry<String, String>> result = answers.entrySet();
		for (Entry<String, String> entry : result) {
			answer.append(entry.getKey()).append(":").append(entry.getValue())
					.append("\n");
		}
		return answer.toString();
	}

	public String getExamName() {
		return examName;
	}
}
