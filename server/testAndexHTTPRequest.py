# -*- coding: utf-8 -*-
import json
import urllib


# exemple d'utilisation d'une requete sur le server android
# pour lancer le serveur : python andexserv.py 127.0.0.1:6112 .
# pour lancer le script : python testAndexHHTPRequest.py


## cmd : command. 1 for get, 2 for submit
cmd = 2

###########
# CONNECT #
###########


#
# Attention Eratum /!\ : Connect est une méthode POST, et non GET (reporté sur le wiki)
#

# première étape : la connexion au serveur

#params : paramètre à indiquer à la méthode connect -> username et password
params = urllib.urlencode({'username':'user','password':'pass'})
print params
#connexion : url + port + /connect, avec le passage d'argument
answer=urllib.urlopen('http://127.0.0.1:6112/connect',params).read()


#la connexion répond avec un string encodé en jspn, qui contient le token et un liste d'examen disponible pour notre utilisateur.
#on charge notre objet json
json_obj = json.loads(answer)

#on accede aux champs qui nous interesse

#token : clé d'identification pour le reste des requetes
token = json_obj['token']

#liste des exams disponibles
exams = json_obj['exams']


#print token
exam = exams[0]


###########
#   GET   #
###########
if cmd == 1 :
	#connexion : url + port + /connect?token=tokenValue&exam=examValue (methode GET)
	url ='http://127.0.0.1:6112/exam?token='+token+'&exam='+exam
	
	# on récupère les informations dans un string contenant un fichier binaire (une archive .zip (?))
	answer=urllib.urlopen(url).read()
	
	# on écrit le contenu dans un fichier [exam].zip
	f = open(exam+".zip", "w")
	f.write(answer)
	
	# close me, please.
	f.close()
if cmd == 2 :
	exam = "javaExam1"
	file = open("sendExam.txt", "r")
	
	
	params = {}
	params["token"] = token
	params["exam"] = exam
	params["data"] = file.read()
	
	#print params["data"]
	
	params = urllib.urlencode(params)
	print urllib.urlopen('http://127.0.0.1:6112/submit',params).read()
	
	